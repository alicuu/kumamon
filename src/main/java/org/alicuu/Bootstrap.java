package org.alicuu;

/**
 * Created by IntelliJ IDEA.
 * User: Danny
 * Date: Date: 16/07/06
 * Time: 09:55
 */
public interface Bootstrap {
    void init(Kumamon kumamon);
}
