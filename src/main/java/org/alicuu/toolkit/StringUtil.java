package org.alicuu.toolkit;

/**
 * Created by IntelliJ IDEA.
 * User: Danny
 * Date: 16/07/05
 * Time: 21:30
 */
public class StringUtil {
    public static final String VAR_REGEXP = ":(\\w+)";
    public static final String VAR_REPLACE = "([^#/?]+)";

    public static boolean isBlank(String str) {
        return null == str || "".equals(str.trim());
    }

    public static String fixPath(String path) {
        if (path == null)
            return "/";
        if (!path.startsWith("/"))
            path = "/" + path;
        if (path.length() > 1 && path.endsWith("/"))
            path = path.substring(0, path.length() - 1);

        return path;
    }


    public static String firstCharUpperCase(String str) {
        return str.toUpperCase().substring(0, 1) + str.substring(1);
    }
}
