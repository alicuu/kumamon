package org.alicuu;

import org.alicuu.mvc.servlet.Request;
import org.alicuu.mvc.servlet.Response;

import javax.servlet.ServletContext;

/**
 * Created by IntelliJ IDEA.
 * User: Danny
 * Date: 16/07/06
 * Time: 10:13
 * 当前线程上下文环境
 */
public class KumamonContext {
    private static final ThreadLocal<KumamonContext> CONTEXT = new ThreadLocal<>();

    private ServletContext context;
    private Request request;
    private Response response;


    private KumamonContext() {
    }

    public static KumamonContext getInstance() {
        return CONTEXT.get();
    }

    static void initContext(ServletContext context, Request request, Response response) {
        KumamonContext kumamonContext = new KumamonContext();
        kumamonContext.context = context;
        kumamonContext.request = request;
        kumamonContext.response = response;
        CONTEXT.set(kumamonContext);
    }


    public static void remove() {
        CONTEXT.remove();
    }

    public ServletContext getContext() {
        return context;
    }

    public void setContext(ServletContext context) {
        this.context = context;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

}
