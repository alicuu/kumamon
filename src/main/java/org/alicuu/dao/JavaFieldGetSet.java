package org.alicuu.dao;

/**
 * Created by IntelliJ IDEA.
 * User: Danny
 * Date: 16/07/06
 * Time: 18:16
 */
public class JavaFieldGetSet {
    private String fieldInfo;
    private String getInfo;
    private String setInfo;

    public JavaFieldGetSet(String fieldInfo, String getInfo, String setInfo) {
        this.fieldInfo = fieldInfo;
        this.getInfo = getInfo;
        this.setInfo = setInfo;
    }

    JavaFieldGetSet() {
    }

    String getFieldInfo() {
        return fieldInfo;
    }

    void setFieldInfo(String fieldInfo) {
        this.fieldInfo = fieldInfo;
    }

    String getGetInfo() {
        return getInfo;
    }

    void setGetInfo(String getInfo) {
        this.getInfo = getInfo;
    }

    String getSetInfo() {
        return setInfo;
    }

    void setSetInfo(String setInfo) {
        this.setInfo = setInfo;
    }

    @Override
    public String toString() {
        return "JavaFieldGetSet{" +
                "fieldInfo='" + fieldInfo + '\'' +
                ", getInfo='" + getInfo + '\'' +
                ", setInfo='" + setInfo + '\'' +
                '}';
    }
}
