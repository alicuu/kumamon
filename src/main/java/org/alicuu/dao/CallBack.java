package org.alicuu.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created by IntelliJ IDEA.
 * User: Danny
 * Date: 16/07/06
 * Time: 18:58
 */
public interface CallBack {
    public Object doExecute(Connection connection, PreparedStatement statement, ResultSet rs);
}
