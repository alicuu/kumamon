package org.alicuu.dao;

/**
 * Created by IntelliJ IDEA.
 * User: Danny
 * Date: 16/07/06
 * Time: 18:10
 */
public class Configuration {

    private String driver;
    private String url;
    private String user;
    private String password;
    private String dBVendor;
    private String srcPath;
    private String persistentObjPackage;
    private int poolMinSize;
    private int poolMaxSize;
    private String queryClass;

    public Configuration(String driver, String url, String user, String password, String dBVendor, String srcPath, String persistentObjPackage) {
        this.driver = driver;
        this.url = url;
        this.user = user;
        this.password = password;
        this.dBVendor = dBVendor;
        this.srcPath = srcPath;
        this.persistentObjPackage = persistentObjPackage;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getdBVendor() {
        return dBVendor;
    }

    public void setdBVendor(String dBVendor) {
        this.dBVendor = dBVendor;
    }

    public String getSrcPath() {
        return srcPath;
    }

    public void setSrcPath(String srcPath) {
        this.srcPath = srcPath;
    }

    public String getPersistentObjPackage() {
        return persistentObjPackage;
    }

    public void setPersistentObjPackage(String persistentObjPackage) {
        this.persistentObjPackage = persistentObjPackage;
    }

    public int getPoolMinSize() {
        return poolMinSize;
    }

    public void setPoolMinSize(int poolMinSize) {
        this.poolMinSize = poolMinSize;
    }

    public int getPoolMaxSize() {
        return poolMaxSize;
    }

    public void setPoolMaxSize(int poolMaxSize) {
        this.poolMaxSize = poolMaxSize;
    }

    public String getQueryClass() {
        return queryClass;
    }

    public void setQueryClass(String queryClass) {
        this.queryClass = queryClass;
    }
}
