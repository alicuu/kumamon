package org.alicuu.dao;

/**
 * Created by IntelliJ IDEA.
 * User: Danny
 * Date: 16/07/06
 * Time: 18:06
 */
public class ColumnInfo {
    public static final int NORMAL_KEY = 0;
    static final int PRIMARY_KEY = 1;
    public static final int FOREIGN_KEY = 2;


    private String colName;
    private String dataType;
    private int keyType;

    ColumnInfo(String colName, String dataType, int keyType) {
        this.colName = colName;
        this.dataType = dataType;
        this.keyType = keyType;
    }

    public String getColName() {
        return colName;
    }

    public void setColName(String colName) {
        this.colName = colName;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public int getKeyType() {
        return keyType;
    }

    public void setKeyType(int keyType) {
        this.keyType = keyType;
    }
}
