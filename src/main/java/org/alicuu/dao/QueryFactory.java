package org.alicuu.dao;

/**
 * Created by IntelliJ IDEA.
 * User: Danny
 * Date: 16/07/10
 * Time: 22:47
 */
public class QueryFactory {
    private static final QueryFactory factory = new QueryFactory();
    private static Query protoTypeObj;

    static {
        try {
            Class clazz = Class.forName(DBManager.getConf().getQueryClass());
            protoTypeObj = (Query) clazz.newInstance();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }
    }

    private QueryFactory() {
    }

    public static QueryFactory getInstance() {
        return factory;
    }

    public Query createFactory() {
        try {
            return (Query) protoTypeObj.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }
}
