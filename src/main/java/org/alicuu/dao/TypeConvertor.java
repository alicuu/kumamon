package org.alicuu.dao;

/**
 * Created by IntelliJ IDEA.
 * User: Danny
 * Date: 16/07/06
 * Time: 19:55
 */
public interface TypeConvertor {
    String databaseTypeToJavaType(String columnType);

    String javaTypeToDatabaseType(String javaType);
}
