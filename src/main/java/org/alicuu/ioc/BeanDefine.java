package org.alicuu.ioc;

/**
 * Created by IntelliJ IDEA.
 * User: Danny
 * Date: 16/07/04
 * Time: 22:29
 */
class BeanDefine {
    private Object bean;
    private Class<?> type;
    private boolean isSingleton;

    BeanDefine(Object bean) {
        this(bean, bean.getClass());
    }

    BeanDefine(Object bean, Class<?> type) {
        this.bean = bean;
        this.type = type;
        this.isSingleton = true;
    }

    BeanDefine(Object bean, Class<?> type, boolean isSingleton) {
        this.bean = bean;
        this.type = type;
        this.isSingleton = isSingleton;
    }

    Object getBean() {
        return bean;
    }

    void setBean(Object bean) {
        this.bean = bean;
    }

    Class<?> getType() {
        return type;
    }

    void setType(Class<?> type) {
        this.type = type;
    }

    boolean isSingleton() {
        return isSingleton;
    }

    void setSingleton(boolean singleton) {
        isSingleton = singleton;
    }
}
