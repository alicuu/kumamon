package org.alicuu.ioc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: Danny
 * Date: 16/07/04
 * Time: 23:00
 */
public class KumamonIoc implements Ioc {

    private static final Logger log = LoggerFactory.getLogger(Ioc.class);
    private final Map<String, BeanDefine> pool = new HashMap<>(32);

    @Override
    public void addBean(Object bean) {
        addBean(bean.getClass().getName(), bean);
    }

    public void addBean(Class<?> beanClass, Object bean) {
        addBean(beanClass.getName(), bean);
    }


    @Override
    public <T> T addBean(Class<T> type) {
        Object bean = addBean(type, true);
        return type.cast(bean);
    }

    private Object addBean(Class<?> type, boolean singleton) {
        return addBean(type.getName(), type, singleton);
    }

    @Override
    public void addBean(String name, Object bean) {
        BeanDefine beanDefine = new BeanDefine(bean);
        addBean(name, beanDefine);

        Class<?>[] interfaces = beanDefine.getType().getInterfaces();
        if (interfaces.length > 0) {
            for (Class<?> interfaceClazz : interfaces) {
                addBean(interfaceClazz.getName(), beanDefine);
            }
        }
    }

    private Object addBean(String name, Class<?> beanClass, boolean singleton) {
        BeanDefine beanDefine = getBeanDefine(beanClass, singleton);
        if (pool.put(name, beanDefine) != null)
            log.warn("Duplicated Bean:{}", name);

        Class<?>[] interfaces = beanClass.getInterfaces();
        if (interfaces.length > 0) {
            for (Class<?> interfaceClazz : interfaces) {
                if (null != getBean(interfaceClazz))
                    break;
                addBean(interfaceClazz.getName(), beanDefine);
            }
        }

        return beanDefine.getBean();
    }


    private void addBean(String name, BeanDefine beanDefine) {
        if (pool.put(name, beanDefine) != null)
            log.warn("Duplicated Bean: {}", name);
    }

    @Override
    public void setBean(Class<?> type, Object proxyBean) {
        BeanDefine beanDefine = pool.get(type.getName());
        if (beanDefine != null)
            beanDefine.setBean(proxyBean);
        else
            beanDefine = new BeanDefine(proxyBean, type);

        pool.put(type.getName(), beanDefine);
    }

    @Override
    public Object getBean(String name) {
        BeanDefine beanDefine = pool.get(name);
        if (beanDefine == null)
            return null;
        return beanDefine.getBean();
    }

    @Override
    public <T> T getBean(Class<T> type) {
        Object bean = getBean(type.getName());
        return type.cast(bean);
    }

    @Override
    public List<BeanDefine> getBeanDefines() {
        return new ArrayList<>(pool.values());
    }

    @Override
    public BeanDefine getBeanDefine(Class<?> type) {
        return getBeanDefine(type, true);
    }

    private BeanDefine getBeanDefine(Class<?> beanClass, boolean singleton) {
        try {
            Object object = beanClass.newInstance();
            return new BeanDefine(object, beanClass, singleton);
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public List<Object> getBeans() {
        Set<String> beanNames = getBeanNames();
        List<Object> beans = new ArrayList<>(beanNames.size());
        for (String beanName : beanNames) {
            Object bean = getBean(beanName);
            if (null != bean)
                beans.add(bean);
        }

        return beans;
    }

    @Override
    public Set<String> getBeanNames() {
        return pool.keySet();
    }

    @Override
    public void remove(Class<?> type) {
        pool.remove(type.getSimpleName());
    }

    @Override
    public void remove(String beanName) {
        pool.remove(beanName);
    }

    @Override
    public void clearAll() {
        pool.clear();
    }
}
