package org.alicuu.ioc.reader;

import java.lang.annotation.Annotation;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * User: Danny
 * Date: 16/07/04
 * Time: 21:38
 */
public interface ClassReader {
    Set<ClassInfo> getClass(String packageName, boolean recursive);

    Set<ClassInfo> getClass(String packageNamw, Class<?> parent, boolean recursive);

    Set<ClassInfo> getClassByAnnotation(String packageName, Class<? extends Annotation> annotation, boolean recursive);


    Set<ClassInfo> getClassByAnnotation(String packageName, Class<?> parent, Class<? extends Annotation> annotation, boolean recursive);
}
