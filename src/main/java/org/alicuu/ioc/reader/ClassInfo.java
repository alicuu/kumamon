package org.alicuu.ioc.reader;

/**
 * Created by IntelliJ IDEA.
 * User: Danny
 * Date: 16/07/04
 * Time: 21:38
 */
public class ClassInfo {
    private String className;
    private Class<?> clazz;

    public ClassInfo(String className) {
        this.className = className;
    }

    ClassInfo(Class<?> clazz) {
        this.clazz = clazz;
        this.className = clazz.getName();
    }

    public String getClassName() {
        return className;
    }

    public Class<?> getClazz() {
        return clazz;
    }

    public Object newInstance() {
        try {
            return clazz.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Override
    public String toString() {
        return clazz.toString();
    }
}
