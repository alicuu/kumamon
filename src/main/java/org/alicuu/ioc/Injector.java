package org.alicuu.ioc;

/**
 * Created by IntelliJ IDEA.
 * User: Danny
 * Date: 16/07/04
 * Time: 22:59
 */
public interface Injector {
    void injection(Object bean);
}
