package org.alicuu.mvc.render;

import java.io.Writer;

/**
 * Created by IntelliJ IDEA.
 * User: Danny
 * Date: 16/07/06
 * Time: 09:24
 */
public interface Render {
    void render(String view, Writer writer);
}
