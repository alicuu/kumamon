package org.alicuu.mvc.render;

import org.alicuu.Const;
import org.alicuu.Kumamon;

import java.io.Writer;

/**
 * Created by IntelliJ IDEA.
 * User: Danny
 * Date: 16/07/06
 * Time: 09:24
 */
public class JspRender implements Render {
    @Override
    public void render(String view, Writer writer) {
        String viewPath = this.getViewPath(view);
    }

    private String getViewPath(String view) {
        Kumamon kumamon = Kumamon.getInstance();
        String viewPrefix = kumamon.getConf(Const.VIEW_PREFIX_FIELD);
        String viewSuffix = kumamon.getConf(Const.VIEW_SUFFIX_FIELD);

        if (viewPrefix == null || viewPrefix.equals(""))
            viewPrefix = Const.VIEW_PREFIX;
        if (viewSuffix == null || viewSuffix.equals(""))
            viewSuffix = Const.VIEW_SUFFIX;

        String viewPath = viewPrefix + "/" + view;
        if (!view.endsWith(viewSuffix))
            viewPath += viewSuffix;

        return viewPath.replaceAll("[/]+", "/");
    }
}
