package org.alicuu.mvc.servlet;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by IntelliJ IDEA.
 * User: Danny
 * Date: 16/07/06
 * Time: 09:31
 */
public class Request {
    private HttpServletRequest rawRequest;


    public Request(HttpServletRequest rawRequest) {
        this.rawRequest = rawRequest;
    }

    public HttpServletRequest getRawRequest() {
        return rawRequest;
    }

    public void setAttr(String name, Object value) {
        rawRequest.setAttribute(name, value);
    }

    public <T> T getAttr(String name) {
        Object value = rawRequest.getAttribute(name);
        return value == null ? null : (T) value;
    }

    public String query(String name) {
        return rawRequest.getParameter(name);
    }


    public String cookie(String name) {
        Cookie[] cookies = rawRequest.getCookies();
        if (null != cookies && cookies.length > 0) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(name))
                    return cookie.getValue();
            }
        }
        return null;
    }
}
