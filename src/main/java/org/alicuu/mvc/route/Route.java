package org.alicuu.mvc.route;

import java.lang.reflect.Method;

/**
 * Created by IntelliJ IDEA.
 * User: Danny
 * Date: 16/07/05
 * Time: 21:45
 */
public class Route {
    private String path;
    private Method action;
    private Object controller;

    Route() {
    }

    String getPath() {
        return path;
    }

    void setPath(String path) {
        this.path = path;
    }

    public Method getAction() {
        return action;
    }

    void setAction(Method action) {
        this.action = action;
    }

    public Object getController() {
        return controller;
    }

    void setController(Object controller) {
        this.controller = controller;
    }
}
