package org.alicuu.mvc.route;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Danny
 * Date: 16/07/05
 * Time: 21:49
 */
public class Routers {
    private static final Logger log = LoggerFactory.getLogger(Routers.class);

    private List<Route> routes = new ArrayList<>();

    public Routers() {
    }

    public void addRoute(List<Route> routes) {
        routes.addAll(routes);
    }

    public void addRoute(Route route) {
        routes.add(route);
    }

    public void removeRoute(Route route) {
        routes.remove(route);
    }

    public void addRoute(String path, Method action, Object controller) {
        Route route = new Route();
        route.setPath(path);
        route.setAction(action);
        route.setController(controller);

        routes.add(route);
        log.info("Add Route:[" + path + "]");
    }

    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }
}
