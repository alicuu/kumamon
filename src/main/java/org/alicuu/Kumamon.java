package org.alicuu;

import org.alicuu.config.ConfigLoader;
import org.alicuu.mvc.render.JspRender;
import org.alicuu.mvc.render.Render;
import org.alicuu.mvc.route.Routers;
import org.alicuu.mvc.servlet.Request;
import org.alicuu.mvc.servlet.Response;

import java.lang.reflect.Method;

/**
 * Created by IntelliJ IDEA.
 * User: Danny
 * Date: 16/07/06
 * Time: 09:18
 */
public final class Kumamon {
    private Routers routers;
    private ConfigLoader configLoader;
    private boolean init = false;
    private Render render;

    public Kumamon() {
        this.routers = new Routers();
        this.configLoader = new ConfigLoader();
        this.render = new JspRender();
    }

    public static Kumamon getInstance() {
        return KumamonHolder.instance;
    }

    boolean isInit() {
        return init;
    }

    void setInit(boolean init) {
        this.init = init;
    }

    public Kumamon loadConf(String conf) {
        configLoader.load(conf);
        return this;
    }

    public Kumamon setConf(String name, String value) {
        configLoader.setConf(name, value);
        return this;
    }

    public String getConf(String name) {
        return configLoader.getConf(name);
    }

    public Kumamon addRoutes(Routers routers) {
        this.routers.addRoute(routers.getRoutes());
        return this;
    }

    Routers getRouters() {
        return routers;
    }

    public Kumamon addRoute(String path, String methodName, Object controller) {
        try {
            Method method = controller.getClass().getMethod(methodName, Request.class, Response.class);
            routers.addRoute(path, method, controller);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return this;
    }

    public Render getRender() {
        return render;
    }

    public void setRender(Render render) {
        this.render = render;
    }

    private static class KumamonHolder {
        private static Kumamon instance = new Kumamon();
    }
}
