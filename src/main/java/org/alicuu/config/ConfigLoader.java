package org.alicuu.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * User: Danny
 * Date: 16/07/06
 * Time: 09:19
 */
public class ConfigLoader {
    private Map<String, Object> configMap;

    public ConfigLoader() {
        this.configMap = new HashMap<>();
    }

    public void load(String conf) {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(new File(conf)));
            toMap(properties);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getConf(String name) {
        Object value = configMap.get(name);
        return value == null ? null : value.toString();
    }

    public void setConf(String name, Object value) {
        configMap.put(name, value);
    }

    private Object getObject(String name) {
        return configMap.get(name);
    }


    private void toMap(Properties properties) {
        if (null != properties) {
            Set<Object> keys = properties.keySet();
            for (Object key : keys) {
                Object value = properties.get(key);
                configMap.put(key.toString(), value);
            }
        }
    }
}
